package com.devcamp.task5820.readlistdrink.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task5820.readlistdrink.model.CDrink;

public interface IDrinkRepository extends JpaRepository <CDrink, Long> {
    
}
