package com.devcamp.task5820.readlistdrink;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication

public class ReadListDrinkApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReadListDrinkApplication.class, args);
	}

}
